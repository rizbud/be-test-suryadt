import bcrypt from "bcrypt";
import db from "../utils/prisma";
import { revokeTokens } from "./auth.service";
import moment from "moment-timezone";

import { Location, User } from "@prisma/client";

export interface IUserPayload {
  email: string;
  password: string;
  first_name: string;
  last_name: string;
  birth_date: string;
  location: {
    city: string;
    province: string;
    country: string;
    timezone: string;
  };
}

interface IUser extends User {
  location: Location | null;
}

export const createUser = async (user: IUserPayload) => {
  user.password = bcrypt.hashSync(user.password, 12);

  return await db.user.create({
    data: {
      ...user,
      location: {
        create: user.location,
      },
    },
  });
};

export const findUserByEmail = async (email: string) => {
  return await db.user.findUnique({
    where: {
      email,
    },
  });
};

export const findUserById = async (id: string) => {
  return await db.user.findUnique({
    where: {
      id,
    },
  });
};

export const deleteUser = async (id: string) => {
  await revokeTokens(id);

  return await db.user.delete({
    where: {
      id,
    },
  });
};

// get users with birthday today based on their timezone
export const getUsersWithBirthdayToday = async () => {
  let users: ReadonlyArray<IUser> = [];

  try {
    users = await db.user.findMany({
      include: {
        location: true,
      },
    });
  } catch (error) {
    console.log("error", error);
  }

  const usersWithBirthdayToday = users.filter((user) => {
    const { timezone = "" } = user.location || {};

    const userBirthdate = moment.tz(`${user.birth_date} 09:00`, timezone);
    const now = moment();
    const nowInUserTimezone = now.tz(timezone);
    const isUserBirthday =
      nowInUserTimezone.isSameOrAfter(userBirthdate) &&
      nowInUserTimezone.isSameOrBefore(
        userBirthdate.clone().add(30, "minutes")
      );

    // @ts-ignore
    delete user.password;

    return isUserBirthday;
  });

  return usersWithBirthdayToday;
};
