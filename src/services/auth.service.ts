import db from "../utils/prisma";
import { hashToken } from "../utils/jwt";

export const addRefreshTokenToWhitelist = async (
  jti: string,
  refreshToken: string,
  user_id: string
) => {
  return await db.refreshToken.create({
    data: {
      id: jti,
      token: hashToken(refreshToken),
      user_id,
    },
  });
};

// used to check if the token sent by the client is in the database.
export const findRefreshTokenById = async (id: string) => {
  return await db.refreshToken.findUnique({
    where: {
      id,
    },
  });
};

// soft delete tokens after usage.
export const deleteRefreshToken = async (id: string) => {
  return await db.refreshToken.update({
    where: {
      id,
    },
    data: {
      revoked: true,
    },
  });
};

export const revokeTokens = async (user_id: string) => {
  return await db.refreshToken.updateMany({
    where: {
      user_id,
    },
    data: {
      revoked: true,
    },
  });
};
