import "dotenv/config";
import axios from "axios";
import db from "../utils/prisma";

interface IEmailPayload {
  id: string;
  email: string;
  first_name: string;
  last_name: string;
}

export const sendEmail = async (payload: IEmailPayload) => {
  try {
    const { email, first_name, last_name } = payload;

    const response = await axios.post(process.env.EMAIL_SERVICE_URL || "", {
      email,
      message: `Hey, ${first_name} ${last_name}! It's your birthday!`,
    });

    console.log("Send email success!");

    await db.unsentMessage.deleteMany({
      where: {
        user_id: payload.id,
      },
    });

    return response.data;
  } catch (error: any) {
    console.error("Send email failed!");

    const unsentMessage = await db.unsentMessage.findFirst({
      where: {
        user_id: payload.id,
      },
    });

    if (!unsentMessage) {
      await db.unsentMessage.create({
        data: {
          user_id: payload.id,
        },
      });
    }
  }
};

export const sendUnsentEmails = async () => {
  try {
    const unsentMessages = await db.unsentMessage.findMany({
      include: {
        user: true,
      },
    });

    unsentMessages.forEach(async (unsentMessage) => {
      sendEmail(unsentMessage.user);
    });

    return true;
  } catch (error: any) {
    console.error("Send unsent emails failed! Will retry again later.");

    return false;
  }
};
