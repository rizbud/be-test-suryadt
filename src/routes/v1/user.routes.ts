import { isAuthenticated } from "../../middlewares/auth.middleware";
import { remove } from "../../controllers/user.controller";
import express from "express";

const router = express.Router();

router.delete("/delete", isAuthenticated, remove);

export default router;
