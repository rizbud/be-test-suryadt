import { body, check } from "express-validator";
import { findUserByEmail } from "../services/user.service";
import moment from "moment-timezone";

export const registerValidator = [
  body("first_name")
    .isLength({ min: 3, max: 100 })
    .withMessage("First Name is required.")
    .trim(),
  body("last_name")
    .isLength({ min: 3, max: 100 })
    .withMessage("Last Name is required.")
    .trim(),
  body("email")
    .isLength({ min: 3, max: 100 })
    .withMessage("Email is required.")
    .bail() // stops validation if previous validation fails
    .if(body("email").exists()) // only run the following validations if email exists
    .isEmail()
    .withMessage("Email is not valid.")
    .normalizeEmail()
    .bail() // stops validation if previous validation fails
    .custom(async (value) => {
      const userExist = await findUserByEmail(value);
      if (userExist) throw new Error("Email already exists.");
    }),
  body("password")
    .isLength({ min: 6 })
    .withMessage("Password must be at least 6 characters.")
    .trim(),
  body("confirm_password").custom((value, { req }) => {
    if (value?.trim() !== req.body.password?.trim()) {
      throw new Error("Password confirmation does not match password.");
    }
    return true;
  }),
  body("birth_date")
    .isLength({ min: 10, max: 10 })
    .withMessage("Birth Date is required.")
    .trim()
    .bail()
    // should be YYYY-MM-DD format
    .custom((value) => {
      const regex = /^\d{4}-\d{2}-\d{2}$/;
      if (!regex.test(value)) throw new Error("Birth Date is not valid.");
      return true;
    }),

  body("location.city")
    .isLength({ min: 3, max: 100 })
    .withMessage("City is required.")
    .trim(),
  body("location.province")
    .isLength({ min: 3, max: 100 })
    .withMessage("Province is required.")
    .trim(),
  body("location.country")
    .isLength({ min: 3, max: 100 })
    .withMessage("Country is required.")
    .trim(),
  body("location.timezone")
    .isLength({ min: 3, max: 100 })
    .withMessage("Timezone is required.")
    .trim()
    .bail()
    // check is timezone is valid using moment-timezone
    .custom((value) => {
      if (!moment.tz.zone(value)) throw new Error("Timezone is not valid.");
      return true;
    }),
];
