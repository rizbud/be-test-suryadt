import "dotenv/config";
import jwt from "jsonwebtoken";
import crypto from "crypto";

const { JWT_ACCESS_SECRET = "", JWT_REFRESH_SECRET = "" } = process.env;

interface IJwtPayload {
  id: string;
}

export const generateAccessToken = (user: IJwtPayload) => {
  return jwt.sign(user, JWT_ACCESS_SECRET, {
    expiresIn: "1d",
  });
};

export const generateRefreshToken = (user: IJwtPayload, jti: string) => {
  return jwt.sign(
    {
      id: user.id,
      jti,
    },
    JWT_REFRESH_SECRET,
    {
      expiresIn: "14d",
    }
  );
};

export const generateToken = (user: IJwtPayload, jti: string) => {
  const accessToken = generateAccessToken(user);
  const refreshToken = generateRefreshToken(user, jti);

  return {
    accessToken,
    refreshToken,
  };
};

export const hashToken = (token: string) => {
  return crypto.createHash("sha256").update(token).digest("hex");
};

export const verifyAccessToken = (token: string) => {
  return jwt.verify(token, JWT_ACCESS_SECRET) as jwt.JwtPayload;
};
