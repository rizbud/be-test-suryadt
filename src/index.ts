import express from "express";
import "dotenv/config";
import bodyParser from "body-parser";
import cors from "cors";
import cron from "node-cron";

import routesV1 from "./routes";
import responseJson from "./helpers/response-json";
import { getUsersWithBirthdayToday } from "./services/user.service";
import { sendEmail, sendUnsentEmails } from "./services/email.service";

const app = express();
const port = process.env.APP_PORT || 3000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

app.use("/", routesV1);

// error 404 handler
app.use((req, res) => {
  responseJson(res, 404, {
    message: "Not Found",
  });
});

app.listen(port, () => {
  console.log("Server running on port: " + port);
});

// schedule task every 30 minutes
cron.schedule("*/30 * * * *", async () => {
  try {
    const users = await getUsersWithBirthdayToday();

    users.forEach((user) => {
      sendEmail(user);
    });
  } catch (error) {
    console.error("error", error);
  }
});

// schedule task every 4 hours
cron.schedule("0 */4 * * *", async () => {
  try {
    sendUnsentEmails();
  } catch (error) {
    console.error("error", error);
  }
});
