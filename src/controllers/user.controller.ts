import { deleteUser } from "../services/user.service";
import responseJson from "../helpers/response-json";

import type { Request, Response } from "express";

export const remove = async (req: Request, res: Response) => {
  const { id } = req.body;

  try {
    await deleteUser(id);

    return responseJson(res, 204);
  } catch (error: any) {
    return responseJson(res, 500, {
      message: "Something went wrong.",
    });
  }
};
