import { v4 as uuidv4 } from "uuid";
import { validationResult } from "express-validator";
import { generateToken } from "../utils/jwt";
import { addRefreshTokenToWhitelist } from "../services/auth.service";
import { createUser } from "../services/user.service";
import responseJson from "../helpers/response-json";

import type { Request, Response } from "express";

export const register = async (req: Request, res: Response) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return responseJson(res, 422, { errors: errors.array() });
  }

  const { email, password, first_name, last_name, birth_date, location } =
    req.body;

  try {
    const User = await createUser({
      email,
      password,
      first_name,
      last_name,
      birth_date,
      location,
    });

    const jti = uuidv4();
    const accessToken = generateToken(
      {
        id: User.id,
      },
      jti
    );

    await addRefreshTokenToWhitelist(jti, accessToken.refreshToken, User.id);

    return responseJson(res, 201, {
      message: "User created successfully",
      access_token: accessToken.accessToken,
      refresh_token: accessToken.refreshToken,
    });
  } catch (error: any) {
    return responseJson(res, 500, {
      message: "Something went wrong.",
    });
  }
};
