# BE Assessment

## Getting Started

### Prerequisites

- [Node.js v18.x](https://nodejs.org/en/download/)
- [Yarn](https://yarnpkg.com/en/docs/install)

### Installing

- Clone the repository
- Run `yarn install` to install dependencies
- Run `yarn dev` to start the development server
- Run `yarn build` to build the project
- Run `yarn run:build` to start the production server
