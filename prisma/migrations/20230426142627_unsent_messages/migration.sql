-- CreateTable
CREATE TABLE "UnsentMessage" (
    "id" TEXT NOT NULL,
    "user_id" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "UnsentMessage_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "UnsentMessage" ADD CONSTRAINT "UnsentMessage_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
